# Fake Resume Using GTP-2

## Why I want a fake resume
Many college level cyber security competitions (CyberForce, CCDC) require that all students submit a resume before competing. These resumes are then shared with recruiters from U.S. federal agencies and their military industrial complex partners. I do not want to work for a three letter federal agency or the private corporations they serve, however I also don't want to put any unwanted attention on myself by turning down recruiters. I decided to use this as an excuse for a fun and socially interesting project. The goal for this project is to create a resume that passes automated screening and initial human inspection without revealing any personal information recruiters would not already know but also not giving them any reason to actually want to recruit me. I know my solution to this problem is a little over engineered but it was important to me to gain exposure and experience using new tools and techniques.

## Technical Abstract
I decided that the best way to fool automated resume screening services and humans into thinking my fake resume is legitimate is by generating it with automated text generation. This has the advantage that the reader learns nothing about my personal writing style and the positionalities that it might imply. I chose to go with GPT-2 because it has a great reputation for making authentic looking paragraph text and there are some really user friendly guides and notebooks for training and generating text.

My first attempt (/pdfs/v1/resumeAva.pdf) used the base GPT-2 model without any fine tuning. It gave some entertaining results but didn't look very professional. Also I turned up the temperature way to much.

For my second attempt I wanted to fine tune the model to sound more like a computer scientist's resume. I initially tried fine tuning it with famously self-aggrandizing books like trump's "The Art of The Deal" or Dale Carnegie's "How to Win Friends and Influence People", however the results I got from this really didn't sound that much like a computer science undergrad. My next idea was to just train the model on the voices of actual computer scientists. I considered get data from undergrad student Slack channels, Discord lobbies, or Reddit subreddits but found that I had a hard time getting solid paragraph text from these sources and that the data I was getting wasn't very on topic. Finally I came to the conclusion that I could scrape data from the cs.washington.edu domain. This data has proved to be pretty good because it covers a wide variety of voices (students content in "homes.cs", people talking about UW CSE in "news.cs", teachers talking about there work everywhere, etc.) and it is pretty easy to scrape.

Once I had a good data set I then used it to fine tune my GPT-2 model, generated text based on prompts related to the section of the resume I was generating for, and picked selected excerpts to put in the resume document.

## Data Sourcing Details
In order to get a good data set of how people write on cs.washington.edu I needed to scrape as much natural sounding paragraph text as possible.

My initial attempt was a spider bot written in python using the Requests and BeautifulSoup libraries. This proved to be terribly insufficient as it was very slow (no multi-processing) provided no meta data or information on what it was doing, and got stuck frequently.

My second spider bot uses Scrapy python library. This library is super awesome, powerful, and extensible. I quickly figured out what kinds of pages my bot was getting stuck on and set to work blacklisting them. I added checks to the bot to prevent it from trying to parse pages that were not html or pages that belonged to subdomains (like "gitlab.cs") that were not useful for the kind of data I wanted.

However I reached a point were my bot was getting stuck on stuff like source code repositories for research projects. This was problematic because there would be hundreds or thousands of page that didn't have anything useful on them but were not easily distinguishable from useful pages. I decided that the best way to circumvent this would be to just set a depth limit for my bot so that instead of needing to constantly blacklist certain types of pages I would just give it shorter leash so that it would get into places like that. This reduced the complexity of the data collection phase considerably but a depth of 4 still got me about ~3.5mb of good quality text data.

Building the spider bot with Scrapy was a lot of fun and there is definitely lots of room for improvement in my design.

## Text Generation Details
After I had cleaned my text data a bit I used it to fine tune my GPT-2 model for 1500 iterations at which point the loss was <2. This gave results that were a little bit broken in comparison to normal speech but good enough for what I needed.

The resume template is one of the stock resume templates provide with Microsoft Word.
## Outcome
The final resumes can be found in /pdfs/. Please note that I made the first version in a couple of hours while eating dinner and the second version in a weekend. If you have any feedback I would love to hear it! I don't plan on working on this project in the near future.
