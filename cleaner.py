import sys, string, json, re, scrubadub
from orderedset import OrderedSet

def decode_and_ignore(text):
    return (str(bytearray(p,'utf8'),'ascii', 'ignore') for p in text)

def remove_punctuation(text):
    return (p.translate(str.maketrans('', '', string.punctuation)).replace('\n', ' ') for p in text)

def fold_whitespace(text):
    return (re.sub(r'\s+', ' ', p) for p in text)

def strip(text):
    return (p.strip() for p in text)

def scrub(text):
    return (scrubadub.clean(p) for p in text)

with open(sys.argv[1], 'rb') as dirty:
    with open(sys.argv[2], 'wb') as clean:
        dirty_json = json.loads(dirty.read())
        # we use a set because we want to remove any common bits of site nav /
        # footer that might appear frequently and mess with our model.
        # python doesn't have built in ordered sets so we use dict
        clean_json = OrderedSet()
        for text in (page['text'] for page in dirty_json):
            clean_json = clean_json | OrderedSet(text)

        for text in strip(fold_whitespace(decode_and_ignore(clean_json))):
            clean.write(bytes((text + '\n'), 'utf'))
