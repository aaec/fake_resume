import scrapy, re, string
from scrapy.linkextractors import LinkExtractor

URL_DONT_MATCH = r'(printers)|(/videos/)|(/films/)|(mailto)|(/courses/)|(gitlab)|(password)|(aslstem)|(ftp)|(Photos)'
CONTENT_DONT_MATCH = r'(\|)|(\\)|(\{)|(\})|(\*)|(\^)|(\%)|(\<)|(\>)|(© )'
# Pull all the <p> text that do not have the footer as an ancestor
CSS_SELECT = ''
ANCESTOR_CLASS_DONT_MATCH = 'contains(concat(" ", normalize-space(@class), " "), " footer ")'
XPATH_SELECT = '//p[not(ancestor::*['+ANCESTOR_CLASS_DONT_MATCH+'])]/text()'

class ParapgraphText(scrapy.Spider):
    name = 'ParagraphText'
    allowed_domains = ['cs.washington.edu']
    start_urls = ['http://cs.washington.edu/']

    def parse(self, response):
        # don't bother if the page isn't text / html
        # converting header structure to string and then matching regex is a
        # very bad way to do this.
        if not re.search(r'text/html', str(response.headers)):
            return

        # filter for not empty text passages that don't continue unnatural
        # special characters
        def natural_text(inputs):
            return [out for out in inputs if (len(out) > 10) and not re.search(CONTENT_DONT_MATCH, out)]

        # yield the data from this page
        yield {

            'text': [out for out in natural_text(response.xpath(XPATH_SELECT).getall()) if (len(out) > 10)],
            'url': response.url
        }

        # recurse
        for link in LinkExtractor(allow=(), deny=(URL_DONT_MATCH)).extract_links(response):
            next_url = response.urljoin(link.url)
            yield scrapy.Request(next_url, callback=self.parse)
