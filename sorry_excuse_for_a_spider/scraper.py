import requests, re, sys
from bs4 import BeautifulSoup

visited_links = set()
bad_links = set()
DOMAIN = 'cs.washington.edu'

def crawl(url):
    # Try http first cause it's faster
    try:
        r = requests.get('http://' + url)
        page = BeautifulSoup(r.text, features="html5lib")
    except:
        try:
            r = requests.get('https://' + url)
            page = BeautifulSoup(r.text, 'lxml')
        except:
            # keeping track of what urls cause errors is good for debugging
            print('>>> This url doesnt work :( -> ' + url, file=sys.stderr)
            bad_links.add(url)
            return

    visited_links.add(url)
    to_visit = set()
    # see if hrefs are worth looking at
    for href in (link.get('href') for link in page.find_all('a')):
        if href:
            new_url = href
            # expand a url of the form '/about_us'
            if (re.search(r'^/', new_url)):
                new_url = url + new_url
            if (href.find(DOMAIN) > 0):
                # attempt to remove redundant urls
                new_url = new_url.replace('www.', '').replace('https://', '').replace('http://','')
                if (new_url[-1] == '/'):
                    new_url = new_url[:-1]
                if not new_url in visited_links:
                    to_visit.add(new_url)

    if len(visited_links) % 50 == 0:
        print(f'>>> visited {len(visited_links)} links ;_;', file=sys.stderr)

    for text in page.stripped_strings:
        if len(text) > 50:
            print(text)
    # print('root: ' + url)
    # for new_url in to_visit:
    #     print(new_url)
    # input('this look good chief?')

    # recurse over the next level of urls
    for new_url in to_visit:
        crawl(new_url)


crawl(DOMAIN)
